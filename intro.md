# What is Nix?

nixos.org tells us, that

> Nix is a powerful package manager for Linux and other Unix
> systems that makes package management reliable and reproducible.
> It provides atomic upgrades and rollbacks, side-by-side installation
> of multiple versions of a package, multi-user package management
> and easy setup of build environments.


IMO that's cluttered and unreadable. Let's edit it.

* Nix
  - is a package manager
  - is for Linux and other Unix systems
  - is reproducible

* It provides
  - atomic upgrades
  - rollbacks
  - side-by-side installation of multiple versions
  - multi-user package management
  - setup of build environments

That's a bit better, but of course we would like some base under some of those sentences.

## Nix is a package manager

Just like Apt, Apk, Yum and other package managers, Nix solves a problem of delivering a program to an end user.

|Command|Nix|Apt|
|-|-|-|
|Search|`nix search pkg`|`apt search pkg`| 
|Install|`nix-env -i pkg`|`apt install pkg`| 
|Remove|`nix-env -e pkg`|`apt remove pkg`| 
|Update|`nix-env -u pkg`|`apt upgrade pkg`|
|Update repos|`nix-channel --update`|`apt update`| 

But Nix has some superpowers on it's own.

`nix-shell -p python3 hexedit` drops you to a shell with Python 3 and hexedit.

`nix-copy --to ssh://user@address $(nix-instantiate '<nixpkgs>' -A firefox)` can copy Firefox with all of it's dependencies from another (local?) machine.

While the last one already starts to a bit like bl̯̚a̳ck̤̘̂̇ ma͍͌g̲ͣi̳͈̱c, we will return to it later.


## Nix is reproducible

Though unlike most of other package managers, Nix is acually a pure functional language in disguise!

### Quick intro into Nix lang

Launch `nix repl`, and let's look at some simple Nix expressions:

```nix
2 + 2

fun = a: a + a
fun 12 

if 2 == 2 then "YES" else "WHAT"

```

> `intro.nix` Write a factorial function.
> This task is located in intro.nix file.

### Sets and arrays

Here's a next bit of code to throw into repl.

```nix
# Sets
a = { a = 12; b = 42; c = 4; }
a.b

# Arrays
b = [ 0 42 "Hi" {} ]
builtins.elemAt b 2
builtins.length b

# Also nix supports simple pattern matching
let 
  fun = { a, b, c }: a * b + c;
in
  fun a

{ a = 12; } // { b = 7; }

```

> `intro.nix` Find maximum in an array, and return it.

### More useful operators

```nix
# `with` operator imports contents of a set into current scope.
with { a: 12; b = 42; c = 2; }; a + b + c

# `builtins` is a set with a bunch of functions, so we can use `with` on it.
with builtins;
  map (a: a * 2) (tail [ 1 2 3 4 ])

```

> `intro.nix` Sort an array. You might want to use builtins reference.

# Useful material
* [builtins reference](https://nixos.org/nix/manual/#ssec-builtins)
