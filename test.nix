with builtins; let
    runTests = tests:
        (foldl' ({solved, all}: test: {solved = if test then solved + 1 else solved; all = all + 1; })) { solved = 0; all = 0; } (
            (map ({test, name}: trace "testing ${name}..." (trace "${if test then "[PASSED]\n" else "]FAILED[\n"}" test))) (
                tests
            )
        );
    testsIntro = 
        let 
            m = (import ./intro.nix);
        in
            [
                { name = "factorial 3"; test = (m.factorial 3 == 6); }
                { name = "factorial 1"; test = (m.factorial 1 == 1); }
                { name = "factorial 0"; test = (m.factorial 0 == 1); }
                { name = "max [ 3 2 1 ]"; test = (m.max [3 2 1] == 3); }
                { name = "max [ 4 5 1 2 ]"; test = (m.max [4 5 1 2] == 5); }
                { name = "max [ 0 ]"; test = (m.max [ 0 ] == 0); }
                { name = "sort [ 3 2 1 ]"; test = (m.sort [3 2 1] == [1 2 3]); }
                { name = "sort [ 4 5 1 2 ]"; test = (m.sort [4 5 1 2] == [ 1 2 4 5 ]); }
                { name = "sort [ 0 ]"; test = (m.sort [ 0 ] == [ 0 ]); }
            ];
    results = runTests testsIntro;
in
    "${toString results.solved}/${toString results.all} tests passed"
